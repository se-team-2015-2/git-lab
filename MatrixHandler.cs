﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaboratoryWork2
{
    public static class MatrixHandler
    {
        public static ElementPosition MinElementSecondaryDiagonal(SquareMatrix matrix)
        {      
            int j;
            int iMin = matrix.Size - 1;
            int jMin = 0;
            int min = matrix[iMin, jMin];

            for (int i = matrix.Size - 1; i >= 0; i--)
            {
                j = matrix.Size - i - 1;
                if (matrix[i, j] < min)
                {
                    min = matrix[i, j];
                    iMin = i;
                    jMin = j;
                }
            }

            return new ElementPosition(iMin, jMin);
        }

        public static ElementPosition MinElementBelowSecondaryDiagonal(SquareMatrix matrix)
        {
            int iMin = matrix.Size - 1;
            int jMin = 1;
            int min = matrix[iMin, jMin];

            for (int i = matrix.Size - 1; i >= 1; i--)
            {
                for (int j = matrix.Size - i; j < matrix.Size; j++)
                {
                    if (matrix[i, j] < min)
                    {
                        min = matrix[i, j];
                        iMin = i;
                        jMin = j;
                    }
                }          
            }

            return new ElementPosition(iMin, jMin);
        }

        public static void SwapElements(SquareMatrix matrix, ElementPosition element1, ElementPosition element2)
        {
            int temp = matrix[element1.Row, element1.Column];
            matrix[element1.Row, element1.Column] = matrix[element2.Row, element2.Column];
            matrix[element2.Row, element2.Column] = temp;
        }   
    }
}
