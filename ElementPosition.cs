﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaboratoryWork2
{
    public class ElementPosition
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public ElementPosition(int row, int column)
        {
            Row = row;
            Column = column;
        }
    }
}
