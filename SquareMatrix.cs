﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaboratoryWork2
{
    public class SquareMatrix
    {
        private const int _minValue = -11;
        private const int _maxValue = 9;

        private int[,] _matrix;

        public int Size { get; private set; }
        public int this[int row, int column]
        {
            get { return _matrix[row, column]; }
            set { _matrix[row, column] = value; }
        }

        public SquareMatrix(int size)
        {
            Size = size;
            _matrix = new int[size, size];

            this.RandomFill();
        }

        private void RandomFill()
        {
            var rand = new Random();

            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    _matrix[i, j] = rand.Next(_minValue, _maxValue + 1);
                }
            }
        }
    }
}
