﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaboratoryWork2
{
    class Program
    {
        static void WriteMatrix(SquareMatrix matrix)
        {
            for (int i = 0; i < matrix.Size; i++)
            {
                for (int j = 0; j < matrix.Size; j++)
                {
                    Console.Write("{0,4}", matrix[i, j]);
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            const int MatrixDimenshion = 8;

            SquareMatrix matrix = new SquareMatrix(MatrixDimenshion);

            Console.WriteLine("Сгенерированная матрица:");
            WriteMatrix(matrix);

            ElementPosition minPosOn;
            minPosOn = MatrixHandler.MinElementSecondaryDiagonal(matrix);
            Console.WriteLine("\nПозиция минимального элемента на побочной диагонали:");
            Console.WriteLine("Строка: {0}; Столбец: {1}", minPosOn.Row + 1, minPosOn.Column + 1);
            Console.WriteLine("Величина элемента: {0}", matrix[minPosOn.Row, minPosOn.Column]);

            ElementPosition minPosBelow;
            minPosBelow = MatrixHandler.MinElementBelowSecondaryDiagonal(matrix);
            Console.WriteLine("\nПозиция минимального элемента ниже побочной диагонали:");
            Console.WriteLine("Строка: {0}; Столбец: {1}", minPosBelow.Row + 1, minPosBelow.Column + 1);
            Console.WriteLine("Величина элемента: {0}", matrix[minPosBelow.Row, minPosBelow.Column]);

            MatrixHandler.SwapElements(matrix, minPosBelow, minPosOn);
            Console.WriteLine("\nМатрица после замены элементов:");
            WriteMatrix(matrix);

            Console.ReadKey();
        }
    }
}
